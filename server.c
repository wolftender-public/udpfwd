#include "boilerplate.h"
#include <arpa/inet.h>
#include <sys/ioctl.h>

#define program_error(source) (perror(source),\
                fprintf(stderr,"%s:%d\n",__FILE__,__LINE__),\
                exit(EXIT_FAILURE))

const int backlog_size = 3;

int bind_socket (int fd_socket, const char * address, uint16_t port) {
    // localhost addr
    int loc_addr;
    if (inet_pton (AF_INET, address, &loc_addr) <= 0) program_error ("inet_pton");

    struct sockaddr_in addr;
    memset (&addr, 0, sizeof (struct sockaddr_in));

    // set the socket to listen on all available interfaces
    addr.sin_family = AF_INET; // tcp addr family
    addr.sin_port = htons (port);
    addr.sin_addr.s_addr = loc_addr;

    int t = 1;
    if (setsockopt (fd_socket, SOL_SOCKET, SO_REUSEADDR, &t, sizeof (t))) program_error ("setsockopt");
    if (bind (fd_socket, (struct sockaddr *) &addr, sizeof (struct sockaddr_in)) < 0) program_error ("bind");

    return fd_socket;
}

int initialize_server (uint16_t port) {
    int fd_socket = socket (PF_INET, SOCK_STREAM, 0);
    if (fd_socket < 0) program_error ("socket");

    bind_socket (fd_socket, "127.0.0.1", port);
    if (listen (fd_socket, backlog_size)) program_error ("listen");

    printf ("server is now listening on localhost port %d\n", port);
    return fd_socket;
}

int initialize_udp_socket (const char * address, uint16_t port) {
    int fd_socket = socket (PF_INET, SOCK_DGRAM, 0);
    if (fd_socket < 0) program_error ("socket");

    bind_socket (fd_socket, address, port);

    printf ("datagram socket listening now on %s:%d\n", address, port);
    return fd_socket;
}

// format xxx.xxx.xxx.xxx:xxxxx
int string_to_sockaddr (const char * str, struct sockaddr_in * addr) {
    char ip_addr [17];
    memset (ip_addr, 0, 17);

    int len = strlen (str);

    int colon = 0;
    while (str [colon] != ':' && colon < len - 1) colon++;

    if (colon >= len - 1 && colon <= 16) return -1;
    strncpy (ip_addr, str, colon);

    int address;
    if (inet_pton (AF_INET, ip_addr, &address) <= 0) return -1;

    uint16_t port = atoi (str + colon + 1);

    addr->sin_family = AF_INET;
    addr->sin_port = htons (port);
    addr->sin_addr.s_addr = address;

    return 0;
}

// it is possible to add some fancy length safety here
// but why when we can just require the output to be INET_ADDRSTRLEN + 6
const unsigned int sockaddr_strlen = INET_ADDRSTRLEN + 6;
int sockaddr_to_string (struct sockaddr_in * addr, char * output) {
    char ip_addr [INET_ADDRSTRLEN + 1];
    memset (ip_addr, 0, INET_ADDRSTRLEN + 1);

    inet_ntop (AF_INET, &addr->sin_addr, ip_addr, INET_ADDRSTRLEN + 1);
    snprintf (output, sockaddr_strlen, "%s:%d", ip_addr, ntohs (addr->sin_port));

    return 0;
}

// helper
void extract_arguments (char * message, int message_len, int * argc, char ** argv, int max_args) {
    for (int c = 0; message [c] && c < message_len; ++c) {
        if (message [c] == ' ') {
            message [c] = 0;
            if (*argc < 11) argv [(*argc)++] = message + c + 1;
        }
    }
}

const char * hello_message = "hello\n";
const char * bye_message = "too many clients, sorry :p\n";
const char * unknown_message = "unknown command\n";

volatile sig_atomic_t last_signal = 0;
void generic_handler (int signo) {
    last_signal = signo;
}

const unsigned int max_message = 512;
const int max_connections = 4;
const int max_rules = 10;

typedef struct upd_rule {
    int fd_udp_sock;
    uint16_t in_port;

    int target_count;
    struct sockaddr_in targets [10];
} udp_rule_t;

int main (int argc, char ** argv) {
    if (argc != 2) program_error ("incorrect usage");
    uint16_t port = atoi (argv [1]);

    int fd_socket = initialize_server (port);
    int active_connections = 0;
    int active_rules = 0;

    int on = 1;
    ioctl (fd_socket, FIONBIO, (char *) &on);
    
    // register signal handler
    set_handler (SIGINT, generic_handler);
    set_handler (SIGPIPE, SIG_IGN);

    // setup select
    fd_set set_read;

    int fd_max = fd_socket;
    int res;

    char tcp_message [max_message + 1];
    char udp_message [max_message];

    int fd_clients [max_connections];
    udp_rule_t forwarding_rules [max_rules];

    for (int i = 0; i < max_rules; ++i) forwarding_rules [i].fd_udp_sock = -1;
    for (int i = 0; i < max_connections; ++i) fd_clients [i] = -1;

    while (last_signal != SIGINT) {
        FD_ZERO (&set_read);
        FD_SET (fd_socket, &set_read);

        for (int i = 0; i < max_connections; ++i) {
            if (fd_clients [i] >= 0) FD_SET (fd_clients [i], &set_read);
        }

        for (int i = 0; i < max_rules; ++i) {
            if (forwarding_rules [i].fd_udp_sock >= 0) FD_SET (forwarding_rules [i].fd_udp_sock, &set_read);
        }

        res = select (fd_max + 1, &set_read, NULL, NULL, NULL);

        if (res < 0) {
            if (errno != EINTR){
                program_error ("select");
            } else {
                continue;
            }
        } else if (res == 0) {
            printf ("select timeout!\n");
            break;
        } else { // fds ready
            if (FD_ISSET (fd_socket, &set_read)) { // accept new clients
                int new_fd = accept (fd_socket, NULL, NULL);
                if (new_fd < 0 && errno != EWOULDBLOCK) program_error ("accept");

                printf ("new connection! (%d)\n", active_connections + 1);

                if (active_connections < max_connections) {
                    printf ("connection accepted!\n");

                    active_connections++;
                    if (TEMP_FAILURE_RETRY (write (new_fd, hello_message, strlen (hello_message))) < 0) program_error ("write");
                    FD_SET (new_fd, &set_read);

                    for (int j = 0; j < max_connections; ++j) {
                        if (fd_clients [j] < 0) {
                            fd_clients [j] = new_fd;
                            break;
                        }
                    }

                    if (new_fd > fd_max) fd_max = new_fd;
                } else {
                    printf ("connection discarded!\n");

                    if (TEMP_FAILURE_RETRY (write (new_fd, bye_message, strlen (bye_message))) < 0) program_error ("write");
                    if (close (new_fd) < 0) program_error ("close");
                }
            } else { 
                /**************************************/
                /*               TCP                  */
                /**************************************/

                int cfd = -1;
                for (int i = 0; i < max_connections; ++i) {
                    cfd = fd_clients [i];
                    if (cfd < 0) continue;

                    if (FD_ISSET (cfd, &set_read)) {
                        memset (tcp_message, 0, max_message + 1);
                        res = TEMP_FAILURE_RETRY (recv (cfd, tcp_message, max_message, 0));

                        if (res < 0) { // error
                            if (errno == EWOULDBLOCK || errno == EAGAIN || errno == EINTR) continue;
                            else program_error ("recv");
                        } else if (res == 0) { // socket closed, close the connection
                            if (close (cfd) < 0) program_error ("close");

                            printf ("client disconnected\n");

                            fd_clients [i] = -1;
                            FD_CLR (cfd, &set_read);
                            active_connections--;
                        } else { // message received normally!
                            // handle the command
                            printf ("received command %s\n", tcp_message);

                            /********************************/
                            /*        FWD COMMAND           */
                            /********************************/
                            if (strncmp (tcp_message, "fwd", 3) == 0) {
                                if (active_rules >= max_rules) {
                                    if (TEMP_FAILURE_RETRY (write (cfd, "rule limit reached\n", 20)) < 0) program_error ("write");
                                    continue;
                                }

                                char * tcp_argv [11];
                                int tcp_argc = 0;
                                extract_arguments (tcp_message, max_message, &tcp_argc, tcp_argv, 11);

                                if (tcp_argc < 2) {
                                    if (TEMP_FAILURE_RETRY (write (cfd, "invalid arguments, try fwd <port> (n times <addr:port>)\n", 57)) < 0) program_error ("write");
                                    continue;
                                }

                                // find the slot for the rule
                                int slot = 0;
                                while (forwarding_rules [slot].fd_udp_sock >= 0) slot++;
                                if (slot >= max_rules) continue;

                                uint16_t source_port = atoi (tcp_argv [0]);
                                for (int j = 0; j < max_rules; ++j) {
                                    if (forwarding_rules [j].fd_udp_sock >= 0 && forwarding_rules [j].in_port == source_port) {
                                        if (TEMP_FAILURE_RETRY (write (cfd, "this port is already busy!\n", 28)) < 0) program_error ("write");

                                        slot = -1;
                                        break;
                                    }
                                }

                                if (slot < 0) continue;
                                printf ("adding new rule on slot %d\n", slot);

                                active_rules++;
                                forwarding_rules [slot].in_port = source_port;
                                forwarding_rules [slot].fd_udp_sock = initialize_udp_socket ("127.0.0.1", forwarding_rules [slot].in_port);
                                forwarding_rules [slot].target_count = 0;

                                if (forwarding_rules [slot].fd_udp_sock > fd_max) {
                                    fd_max = forwarding_rules [slot].fd_udp_sock;
                                }

                                for (int j = 1; j < tcp_argc; ++j) { // parse output addresses
                                    struct sockaddr_in target;
                                    if (string_to_sockaddr (tcp_argv [j], &target) < 0) {
                                        if (TEMP_FAILURE_RETRY (write (cfd, "invalid ip input\n", 18)) < 0) program_error ("write");
                                        continue;
                                    }

                                    forwarding_rules [slot].targets [forwarding_rules [slot].target_count++] = target;
                                }

                                if (TEMP_FAILURE_RETRY (write (cfd, "rule added successfully!\n", 26)) < 0) program_error ("write");

                            /********************************/
                            /*        CLOSE COMMAND           */
                            /********************************/
                            } else if (strncmp (tcp_message, "close", 5) == 0) {
                                char * tcp_argv [11];
                                int tcp_argc = 0;
                                extract_arguments (tcp_message, max_message, &tcp_argc, tcp_argv, 1);

                                if (tcp_argc != 1) {
                                    if (TEMP_FAILURE_RETRY (write (cfd, "invalid usage, try close <port>\n", 33)) < 0) program_error ("write");
                                    continue;
                                }

                                uint16_t close_port = atoi (tcp_argv [0]);

                                int rule_found = 0;
                                for (int j = 0; j < max_rules; ++j) {
                                    if (forwarding_rules [j].in_port == close_port && forwarding_rules [j].fd_udp_sock >= 0) {
                                        if (close (forwarding_rules [j].fd_udp_sock) < 0) program_error ("close");

                                        forwarding_rules [j].fd_udp_sock = -1;
                                        forwarding_rules [j].target_count = 0;
                                        forwarding_rules [j].in_port = 0;
                                        active_rules--;

                                        if (TEMP_FAILURE_RETRY (write (cfd, "port closed successfully!\n", 27)) < 0) program_error ("write");
                                        rule_found = 1;
                                    }
                                }

                                if (!rule_found) {
                                    if (TEMP_FAILURE_RETRY (write (cfd, "there are no rules for requested port\n", 39)) < 0) program_error ("write");
                                }

                            /********************************/
                            /*        SHOW COMMAND           */
                            /********************************/
                            } else if (strncmp (tcp_message, "show", 4) == 0) {
                                if (active_rules > 0) {
                                    if (TEMP_FAILURE_RETRY (write (cfd, "displaying all active rules\n", 29)) < 0) program_error ("write");
                                    char buffer [512], addr_buffer [sockaddr_strlen + 1];

                                    for (int j = 0; j < max_rules; ++j) {
                                        if (forwarding_rules [j].fd_udp_sock > 0) { // is an active rule
                                            memset (buffer, 0, 512);
                                            snprintf (buffer, 512, "fwd %d to ", forwarding_rules [j].in_port);

                                            for (int k = 0; k < forwarding_rules [j].target_count; ++k) {
                                                memset (addr_buffer, 0, sockaddr_strlen + 1);
                                                sockaddr_to_string (&forwarding_rules [j].targets [k], addr_buffer);

                                                strcat (buffer, addr_buffer);
                                                if (k + 1 < forwarding_rules [j].target_count) strcat (buffer, " ");
                                            }

                                            strcat (buffer, "\n");
                                            if (TEMP_FAILURE_RETRY (write (cfd, buffer, strlen (buffer))) < 0) program_error ("write");
                                        }
                                    }
                                } else {
                                    if (TEMP_FAILURE_RETRY (write (cfd, "no active rules to display\n", 28)) < 0) program_error ("write");
                                }
                            } else {
                                if (TEMP_FAILURE_RETRY (write (cfd, unknown_message, strlen (unknown_message))) < 0) program_error ("write");
                            }
                        }
                    }
                }

                /**************************************/
                /*               UDP                  */
                /**************************************/

                cfd = -1;
                for (int i = 0; i < max_rules; ++i) {
                    cfd = forwarding_rules [i].fd_udp_sock;
                    if (cfd < 0) continue;

                    udp_rule_t * rule = &forwarding_rules [i];

                    // read udp datagram and forward it
                    if (FD_ISSET (cfd, &set_read)) {
                        memset (udp_message, 0, max_message);
                        res = TEMP_FAILURE_RETRY (recv (cfd, &udp_message, max_message, 0));

                        if (res < 0) {
                            if (errno == EWOULDBLOCK || errno == EAGAIN || errno == EINTR) continue;
                            else program_error ("recv");
                        } else if (res > 0) {
                            // message was read, forward it
                            // res is the number of bytes received

                            for (int j = 0; j < rule->target_count; ++j) {
                                if (TEMP_FAILURE_RETRY (sendto (rule->fd_udp_sock, udp_message, res, 0, &rule->targets [j], sizeof (struct sockaddr_in))) < 0) {
                                    program_error ("sendto");
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // close all connections
    for (int i = 0; i <= fd_max; ++i) {
        if (FD_ISSET (i, &set_read)) {
            close (i);
        }
    }

    return EXIT_SUCCESS;
}