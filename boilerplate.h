#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <netdb.h>
#include <fcntl.h>

ssize_t bulk_read (int fd, char *buf, size_t count) {
        int c;
        size_t len = 0;

        do {
                c = TEMP_FAILURE_RETRY (read (fd, buf, count));
                if (c < 0) return c;
                if (c == 0) return len;
                buf += c;
                len += c;
                count -= c;
        } while (count>0);

        return len;
}
ssize_t bulk_write (int fd, char *buf, size_t count) {
        int c;
        size_t len = 0;

        do {
                c = TEMP_FAILURE_RETRY (write (fd, buf, count));
                if (c < 0) return c;
                buf += c;
                len += c;
                count -= c;
        } while(count > 0);

        return len;
}

int set_handler (int signo, void (*f) (int)) {
    struct sigaction act;
    memset (&act, 0, sizeof (struct sigaction));

    act.sa_handler = f;

    if (sigaction (signo, &act, NULL) == -1)
        return -1;

    return 0;
}